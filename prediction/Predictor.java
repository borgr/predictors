package prediction;

import org.powertac.common.WeatherForecast;
import org.powertac.common.WeatherReport;

public interface Predictor {
//	public void processInfo(WeatherForecast wf);//TODO is there another info we need?
//	public void processInfo(WeatherReport wr);
	/**
	 * processes an amount produces in a certain timeslot
	 * @param production
	 * @param timeSlot
	 */
	public void processInfo(double production, int timeSlot);//TODO find out how do we get it (maybe in one object?)
	/**
	 * returns the predicted production in the timeslot per pupulation unit
	 * @param timeSlot
	 * @return
	 */
	public double predict(int timeSlot);
}
