package org.powertac.samplebroker;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.powertac.common.WeatherForecast;
import org.powertac.common.WeatherForecastPrediction;
import org.powertac.common.WeatherReport;
import org.powertac.samplebroker.PortfolioManagerService;

/**
 * an abstract class for combining needed elements for predictions based on weather
 * @author borgr
 *
 */
public abstract class WeatherPredictor implements Predictor {
	static protected Logger log = Logger.getLogger(WeatherPredictor.class);
	//saves parameters seen in a <timeslot, prameter> hashmaps
	static protected HashMap<Integer, Double> speedsSeen = new HashMap<Integer, Double>();
	static protected HashMap<Integer, Double> directionsSeen = new HashMap<Integer, Double>();
	static protected HashMap<Integer, Double> temperaturesSeen = new HashMap<Integer, Double>();
	static protected HashMap<Integer, Double> cloudsSeen = new HashMap<Integer, Double>();
	static protected HashMap<Integer, Integer> slotsInDay = new HashMap<Integer, Integer>();
	static protected WeatherForecast updatedForecast;
	static public final int FAIL = -1000;
	// TODO other weather parameters are not yet implemented
	
	/**
	 * processes a weather forecast message
	 * @param wf
	 */
	static public void processWeatherInfo(WeatherForecast wf, int slotInDay) {
		updatedForecast = wf;
		for (int i = 1; i < 26; i++)
		{
			slotsInDay.put(wf.getTimeslotIndex()+i, (slotInDay+i) % 24);
			log.info("timeslot: " + wf.getTimeslotIndex()+i + " slotInDay:" + ((slotInDay+i) % 24));
		}
//		log.info("forecast is now for" + updatedForecast.getTimeslotIndex());

	}

	/**
	 * processes a weather report
	 * @param wfr
	 */
	static public void processWeatherInfo(WeatherReport wr) {
		speedsSeen.put(wr.getTimeslotIndex(), wr.getWindSpeed());
		directionsSeen.put(wr.getTimeslotIndex(), wr.getWindDirection());
		temperaturesSeen.put(wr.getTimeslotIndex(), wr.getTemperature());
		cloudsSeen.put(wr.getTimeslotIndex(), wr.getCloudCover());

	/**	log.info("abstract weather processed speed: " +
		temperaturesSeen.put(wr.getTimeslotIndex(), wr.getTemperature());
		cloudsSeen.put(wr.getTimeslotIndex(), wr.getCloudCover()); */
		if(speedsSeen.get(wr.getTimeslotIndex()) < 0)
		{
			log.error("negative wind?!");
		}
		if(directionsSeen.get(wr.getTimeslotIndex()) < 0 || directionsSeen.get(wr.getTimeslotIndex()) > 360)
		{
			log.error("invalid direction?!");
		}
		//TODO other parameters are not yet implemented
	}
	
	/**
	 * finds the forecast for the specific timeSlot
	 * @param timeSlot
	 * @return null if unable
	 */
	static public WeatherForecastPrediction getForcast(int timeSlot)
	{
		if (updatedForecast == null || timeSlot < updatedForecast.getTimeslotIndex() || timeSlot >= updatedForecast.getTimeslotIndex() + updatedForecast.getPredictions().size())
		{
			return null;
		}
		return updatedForecast.getPredictions().get(timeSlot - updatedForecast.getTimeslotIndex());
	}
	
	/**
	 * finds the speed forecasted in the timeSlot
	 * @param timeSlot
	 * @return
	 */
	static public double forecastSpeed(int timeSlot)
	{
		WeatherForecastPrediction wfp = getForcast(timeSlot);
		if(wfp == null)
		{
			wfp = getForcast(timeSlot-1);
			if (wfp == null)
			{
				return FAIL;
			}
			return wfp.getWindSpeed();
		}
		return wfp.getWindSpeed();
	}
	
	/**
	 * finds the wind direction forecasted in the timeSlot
	 * @param timeSlot
	 * @return
	 */
	static public double forecastDirection(int timeSlot)
	{
		WeatherForecastPrediction wfp = getForcast(timeSlot);
		if(wfp == null)
		{
			wfp = getForcast(timeSlot-1);
			if (wfp == null)
			{
				return FAIL;
			}
			return wfp.getWindDirection();
		}
		return wfp.getWindDirection();
	}
	
	/**
	 * finds the temperature forecasted in the timeSlot
	 * @param timeSlot
	 * @return
	 */
	static public double forecastTemperature(int timeSlot)
	{
		WeatherForecastPrediction wfp = getForcast(timeSlot);
		if(wfp == null)
		{
			wfp = getForcast(timeSlot-1);
			if (wfp == null)
			{
				return FAIL;
			}
			return wfp.getTemperature();
		}
		return wfp.getTemperature();
	}
	
	/**
	 * finds the cloud cover forecasted in the timeSlot
	 * @param timeSlot
	 * @return
	 */
	static public double forecastCloud(int timeSlot)
	{
		WeatherForecastPrediction wfp = getForcast(timeSlot);
		if(wfp == null)
		{
			wfp = getForcast(timeSlot-1);
			if (wfp == null)
			{
				return FAIL;
			}
			return wfp.getCloudCover();
		}
		return wfp.getCloudCover();
	}
	
	/**
	 * get speed seen OR forecasted
	 * @param timeSlot
	 * @return speed seen at the timeslot
	 */
	static public double getSpeed(int timeSlot)
	{
		if (speedsSeen.containsKey(timeSlot))
		{
			return speedsSeen.get(timeSlot);
		}
		return forecastSpeed(timeSlot);
	}
	
	/**
	 * get direction seen OR forecasted!
	 * @param timeSlot
	 * @return direction seen at the timeslot
	 */
	static public double getDirection(int timeSlot)
	{
		if (directionsSeen.containsKey(timeSlot))
		{
			return directionsSeen.get(timeSlot);
		}
		return forecastDirection(timeSlot);
	}

	/**
	 * get temperature seen OR forecasted!
	 * @param timeSlot
	 * @return direction seen at the timeslot
	 */
	public static double getTemperature(int timeSlot) {
		if (temperaturesSeen.containsKey(timeSlot))
		{
			return temperaturesSeen.get(timeSlot);
		}
		return forecastTemperature(timeSlot);
	}

	/**
	 * get cloud cover seen OR forecasted!
	 * @param timeSlot
	 * @return direction seen at the timeslot
	 */
	public static double getCloud(int timeSlot) {
		if (cloudsSeen.containsKey(timeSlot))
		{
			return cloudsSeen.get(timeSlot);
		}
		return forecastCloud(timeSlot);
	}
	
	/**
	 * Fills the slotInDay parameters for processing the bootstrap data
	 * @param timeSlot
	 * @param slotInDay
	 * @return
	 */
	public static void setSlotInDay(int timeSlot, int slotInDay)
	{
		slotsInDay.put(timeSlot, slotInDay);
	}
	
	public static int getSlotInDay(int timeSlot){
		return slotsInDay.get(timeSlot);
	}
}
