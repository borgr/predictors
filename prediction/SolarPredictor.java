package org.powertac.samplebroker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.jblas.DoubleMatrix;
import org.jblas.Solve;
import org.powertac.common.msg.CustomerBootstrapData;
import org.powertac.common.repo.TimeslotRepo;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * a class for predicting solar production for powerTAC
 * @author borgr
 *
 */
public class SolarPredictor extends WeatherPredictor implements Predictor {
	
	
	static int IDS = 0;
	// all log words: "0prod", "recalc", "cbd", "fullRep", "alterM", "testAlterVec", "alterVec", "updateMat", "predict", calcRange
	static final String[] printing = {"recalc", "cbd", "fullRep", "alterM", "testAlterVec", "predict"};
	private int ID;
	///recalculate after RECALCULATE times
	private final int RECALCULATE = 10;
	///how many recalculations repetitions to do
	private final int ALTER_REPS = 10;
	///hash of timeSlot and production
	private HashMap<Integer, Double> netUsage;
	private int uncalculatedProductions;
	private final double DEFAULT_M = 0;
//	static int[] temperatureSeperators = {100}; //upper bounds of ranges
	static double[] cloudSeperators = {0.3, 0.5, 0.7, 0.8, 0.9,1.0};//TODO change buckets
	static int[] hourSeperators = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
	double m;
	private DoubleMatrix elemCount;
	private DoubleMatrix avg;
	private DoubleMatrix predictionsMat;
	private DoubleMatrix hourFactors;
	private DoubleMatrix cloudFactors;	

	// collects no production timeSlots
	private ArrayList<Integer> prod0Slot;

	//variables for logs, can be deleted
	private double naiveSum = 0;
	private double errorSum = 0;
	private double predCount = 0;

	//if population can be deduced by its own it is not needed, by then it replaces ctor with better API:
	//WindmillPredictor(CustomerBootstrapData cbd)
	public SolarPredictor(CustomerBootstrapData cbd, double population) {
		m = DEFAULT_M;
		hourFactors = new DoubleMatrix(new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.05, 0.31, 0.58, 0.77, 0.92, 1.0, 1.0, 0.92, 0.77, 0.58, 0.31, 0.05, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0});
		cloudFactors= new DoubleMatrix(new double[] {1.0, 0.8, 0.5, 0.35, 0.25, 0.15 });
		prod0Slot = new ArrayList<Integer>();
		predictionsMat = new DoubleMatrix(hourSeperators.length, cloudSeperators.length);
		updatePredictionMat();
		elemCount = DoubleMatrix.zeros(hourSeperators.length, cloudSeperators.length);
		avg = DoubleMatrix.zeros(hourSeperators.length, cloudSeperators.length);
		ID = IDS;
		IDS++;
		netUsage = new HashMap<Integer, Double>();
		double[] boot = cbd.getNetUsage();
		for (int i = 0; i < boot.length; i++)
		{
			if(boot[i] != 0)
			{
				processInfo(boot[i]/population, i);
				WeatherPredictor.setSlotInDay(i, i % 24);
				logIt(boot[i]/population + "boot pop" + population, "cbd");
			}
		}
		//		logIt(toString() + "created successfully");
		uncalculatedProductions = RECALCULATE + 1;

		//TODO delete test
		DoubleMatrix testVec = DoubleMatrix.zeros(3);
		DoubleMatrix A = new DoubleMatrix(new double[] {0,1,1000});
		DoubleMatrix B = new DoubleMatrix(new double[] {10,2,1000});
		double c = m;
		m = 1;
		alter(0, testVec, A , new DoubleMatrix(new double[] {1,1,0}), B);
		logIt("should be [2,0,0]:" + testVec, "testAlterVec");
		m = c;
	}

	/**
	 *  calculates best fit for alterVec[alterIndex] in the equation:
	 *  cellCount*combinedFactorVec = cellCount*constVect * alterVec[alterIndex]
	 *  or C*A = C*B*X
	 *  tested to work for vectors, not guaranteed to work with A B or C matrix 
	 * @param alterIndex place altered in the vector
	 * @param alterVec vector in which a place will be altered
	 * @param constVec 
	 * @param cellCount
	 * @param predictionVec - vector with values to predict
	 */
	private void alter(int alterIndex, DoubleMatrix alterVec,
			DoubleMatrix constVec, DoubleMatrix cellCount,
			DoubleMatrix predictionVec)
	{
		DoubleMatrix constLocal = constVec;
		//		DoubleMatrix countLocal = cellCount;
		//		if(constLocal.isRowVector())
		//		{
		//			constLocal = constVec.transpose();
		//		}
		logIt(toString() + " altering vec with \nelemCount:" + cellCount + " \nvec:" + constLocal + "\nfactors: " + predictionVec, "alterVec");
		logIt("before altering:" + alterVec.get(alterIndex), "alterVec");
		DoubleMatrix A = cellCount.mul(constLocal.mul(m));
		DoubleMatrix B = cellCount.mul(predictionVec);
		logIt("complex multiplication works\nA:" + A + "\nB: " + B, "alterVec");
		logIt("jblas solution:" + Solve.solveLeastSquares(A, B), "alterVec");
		alterVec.put(alterIndex, Solve.solveLeastSquares(A, B).get(0)); 
		logIt("solved cell: " + alterVec.get(alterIndex), "alterVec");
	}

	/**
	 * alters m 
	 * @param alterIndex
	 */
	private void alter()
	{
		if (m == DEFAULT_M)
		{
			//starts with avg as first guess
			m = getAverage();
			updatePredictionMat();
			if(m == Double.NaN)
			{
				m = DEFAULT_M;
			}
			logIt("m initiated to:" + m, "alterM");
		}
		else
		{
			logIt("new m is now: " + m + "\nour m is now:" + predictionsMat.mul(elemCount).sum()/elemCount.sum(), "alterM");
			//		logIt("factors is" + temperatureFactors.mmul(cloudFactors.transpose());
			DoubleMatrix A = getFactorMat().mul(elemCount).reshape(predictionsMat.getLength(), 1);
			DoubleMatrix B = predictionsMat.mul(elemCount).reshape(predictionsMat.getLength(), 1);
			//		logIt("after reshaping they look like:" +A + " factors: " + B);
			m = Solve.solveLeastSquares(A, B).get(0);
			//		logIt("jblas m sol:" + Solve.solveLeastSquares(A, B));
			//	logIt("solving for m\nA:" + getFactorMat() + "\nB: " + predictionsMat + "\nnew m :" + m);
		}
	}
	private void removeNaN(DoubleMatrix mat) {
		for(int i = 0; i < mat.length; i++)
		{
			mat.put(i, Double.isNaN(mat.get(i)) ? 0 : mat.get(i)); 
		}

	}

	/**
	 * combines the temperature and cloud factors with the mean to get a prediction matrix
	 */
	private void updatePredictionMat()
	{
		predictionsMat = getFactorMat();
		predictionsMat.muli(m);
		//	logIt("prediction matrix: " + predictionsMat);
	}

	/**
	 * @return the matrix of temperatureFactors*cloudFactors.transpose()
	 */
	private DoubleMatrix getFactorMat() {
		return hourFactors.mmul(cloudFactors.transpose());
	}

	@Override
	public double predict(int timeSlot) {
		//		logIt(toString() + "predict recalculates? " +(uncalculatedProductions > RECALCULATE) + " uncalculatedProductions " + uncalculatedProductions);

		if(uncalculatedProductions > RECALCULATE)
		{
			reCalculate();
			logIt("full report:" + "\nm:" + m + "\nelemCount:" + elemCount + "\navg:" + avg + "\nprediction mat" + predictionsMat + "\nprediction average error: " +
					(errorSum/predCount) + "\nnaive average error: " + (naiveSum/predCount), "fullRep");
		}


		double result = -1;
		try
		{
			result = predictionsMat.get(WeatherPredictor.getSlotInDay(timeSlot),
					calculateRange(cloudSeperators, WeatherPredictor.getCloud(timeSlot)));
		}
		catch (ArrayIndexOutOfBoundsException e) 
		{
			result = -1;
		}
		//	if you want to get just the average as a prediction
	  //	result = -1;
		if (result == -1)
		{
			logIt(toString() + " unable to predict, probably no forecast was given", "predict", true);
			try
			{
				result = avg.get(WeatherPredictor.getSlotInDay(timeSlot),
						calculateRange(cloudSeperators, WeatherPredictor.getCloud(timeSlot)));
			}

			catch (ArrayIndexOutOfBoundsException e) 
			{
				result = -1;
				logIt(toString() + " unable to predict", "predict", true);
			}
		}
		return (result == -1) ? getAverage() : result;
	}

	/**
	 * 
	 * @return average over all productions seen 
	 */
	private double getAverage() {
		return avg.mul(elemCount).sum()/elemCount.sum();
	}

	/**
	 * calculates the formula that gets temperature and cloud and returns a production
	 */
	private void reCalculate() {
		logIt(toString() + "recalculating formula", "recalc");
		updateMatrices();
		for (int j = 0; j < ALTER_REPS; j++)
		{
			alter();
			assert(avg.rows == hourFactors.length);
			assert(avg.columns == cloudFactors.length);
			elemCount.assertSameSize(avg);
			for (int i = 0; i < cloudFactors.length; i++)
			{
				//			logIt("factors before:" + cloudFactors);
				alter(i, cloudFactors, hourFactors, elemCount.getColumn(i), avg.getColumn(i));
				//			logIt("factors after:" + cloudFactors);
			}
			for (int i = 0; i < hourFactors.length; i++)
			{
				alter(i, hourFactors, cloudFactors, elemCount.getRow(i).transpose(), avg.getRow(i).transpose());
			}
		}

		updatePredictionMat();
		logIt("temperature Factors:" + hourFactors + "cloud Factors" + cloudFactors, "recalc");
		logIt("predictions now: " + predictionsMat, "recalc");
		if (predictionsMat.le(300).min() == 0)
		{
			logIt("big number found", "recalc");
		}
	}

	/**
	 * makes default value info
	 * @see logIt(String string, String logType, boolean error)
	 * @param string
	 * @param logType
	 */
	private void logIt(String string, String logType)
	{
		logIt(string, logType, false);
	}
	/**
	 * logs the string iff logType is in printing array
	 * @param string
	 * @param logType
	 */
	private void logIt(String string, String logType, boolean error) {
		if (Arrays.asList(printing).contains(logType))
		{
			if (error)
			{
				log.error(string);
			}
			else
			{
				log.info(string);
			}
		}

	}

	/**
	 * adds the new information gathered to the needed matrices
	 */
	private void updateMatrices() {
		//	logIt("0 production info:\ntimeSlot\ttemperature\tdir\ttemperatureRange\tdirRane", "0prod");
		//		for (Integer elem : prod0Slot)
		//		{
		///**			logIt("\t" + elem + "\t" + WeatherPredictor.getTemperature(elem) + "\t" + WeatherPredictor.getCloud(elem) + "\t"
		//		+ calculateRange(temperatureSeperators, WeatherPredictor.getTemperature(elem)) + "\t" + calculateRange(cloudSeperators, WeatherPredictor.getCloud((elem))));
		//	*/	}


		// make avg a sum matrix
		logIt("matrix before" + avg, "updateMat");
		avg = avg.mul(elemCount);
		logIt("sum matrix" + avg, "updateMat");
		//add new values
		for (Map.Entry<Integer, Double> entry : netUsage.entrySet())
		{
			try{
				int hourRange = WeatherPredictor.getSlotInDay(entry.getKey());
				int cloudRange = calculateRange(cloudSeperators, WeatherPredictor.getCloud((entry.getKey())));
				// make sure weather report exists
				if(hourRange != WeatherPredictor.FAIL && cloudRange != WeatherPredictor.FAIL)
				{
					//				// logs when temperatureRange is 0 but production is not
					//				 if (temperatureRange == 0 && entry.getValue() != 0)
					// logs when temperatureRange is 0
					if (hourRange == 0)
					{
						/**			logIt(toString() + " added " + entry.getValue() 
							+ " with temperature: " + WeatherPredictor.getTemperature(entry.getKey())
							+ " and cloud: " + WeatherPredictor.getCloud((entry.getKey())) + 
							"\nto cell" +temperatureRange+ "," + cloudRange);*/
					}
					//increment count
					elemCount.put(hourRange, cloudRange, elemCount.get(hourRange, cloudRange) + 1);

					//add new value to the sums matrix
					avg.put(hourRange, cloudRange, avg.get(hourRange, cloudRange) + entry.getValue());

					//TODO delete this zeroing up, it is done later anyway (needed for recursive call for predict)
					//				uncalculatedProductions = 0;
					//				logIt("added weather report for timeSlot" + entry.getKey() + "\nvalue = " + entry.getValue() + "\nwould have predicted:" + predict(entry.getKey()));
					uncalculatedProductions = 0;
					errorSum += entry.getValue() - predict(entry.getKey());
					predCount++;
					naiveSum += avg.get(hourRange,
							calculateRange(cloudSeperators, WeatherPredictor.getCloud(entry.getKey())))/elemCount.get(hourRange,
									calculateRange(cloudSeperators, WeatherPredictor.getCloud(entry.getKey())));				
				}
				else
				{
					logIt("no weather report found for timeSlot" + entry.getKey(), "updateMat", true);
				}
			}
			catch (Exception e)
			{
				logIt("entry error, probably exceeded separator bound", "updateMat", true);
			}
		}
		//		logIt("new sum matrix" + avg);
		//divide the matrix to make it avg matrix again instead of sum matrix
		avg = avg.div(elemCount);
		removeNaN(avg);
		//clean updated values
		allInfoUsed();		
		//	logIt(toString() + "avgs matrix calculated");
		//		logIt(avg);
		//		logIt(elemCount);
	}

	private void allInfoUsed() {
		netUsage.clear();
		uncalculatedProductions = 0;		
	}

	/**
	 * returns the lowest cell that is bigger than than the key
	 * assumes array is sorted 
	 * assumes key is smaller than the biggest cell
	 * @param temperatureSeperators2
	 * @param key
	 * @return number of lowest cell,
	 * WeatherPredictor.FAIL otherwise (the FAIL value is guarenteed not to be a legal num)
	 */
	private int calculateRange(int[] array, double key) {
		if (key < 0)
		{
			//		logIt("no data");
			return WeatherPredictor.FAIL;
		}
		for(int i = 0; i < array.length; i++)
		{
			if (key <= array[i])
			{
				logIt("key smallerequal than" + key + "this"+ array[i], "calcRange");
				return i;
			}
		}
		return WeatherPredictor.FAIL;
	}
	
	private int calculateRange(double[] array, double key) {
		if (key < 0)
		{
			//		logIt("no data");
			return WeatherPredictor.FAIL;
		}
		for(int i = 0; i < array.length; i++)
		{
			if (key <= array[i])
			{
				logIt("key smallerequal than" + key + "this"+ array[i], "calcRange");
				return i;
			}
		}
		return WeatherPredictor.FAIL;
	}

	@Override
	public void processInfo(double production, int timeSlot) {
		uncalculatedProductions++;
		netUsage.put(timeSlot, production);


		if (production == 0)
		{
			prod0Slot.add(timeSlot);
		}

		//		logIt(toString() + "got production info" + netUsage.get(timeSlot) 
		//				+ "in timeSlot" + timeSlot + "uncalculated productions:" + uncalculatedProductions);
	}
	//	@Override
	//	public void processInfo(WeatherForecast wf) {
	//		logIt(toString() + "forcast");
	//		super.processInfo(wf);
	//		
	//
	//	}
	//	@Override
	//	public void processInfo(WeatherReport wf) {
	//		logIt(toString() + "got report");
	//		super.processInfo(wf);
	//		logIt("succesfully, temperature recorded:"+temperaturesSeen.get(wf.getTimeslotIndex()));
	//		
	//		
	//	}
	@Override
	public String toString()
	{
		return "#" + ID + " solar predictor";
	}

}

//
//package prediction;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//
//import org.jblas.DoubleMatrix;
//import org.jblas.Solve;
//import org.powertac.common.msg.CustomerBootstrapData;
//
//
//public class SolarPredictor extends WeatherPredictor implements Predictor {
//	static int IDS = 0;
//	private int ID;
//	private final int RECALCULATE = 10;
//	private final int ALTER_REPS = 10;
//	private HashMap<Integer, Double> netUsage;
//	private int uncalculatedProductions;
//	static int[] temperatureSeperators = {-10,0,15,25,50}; //upper bounds of ranges
//	static int[] cloudSeperators = {100};//TODO change buckets
//	double m;
//	private DoubleMatrix elemCount;
//	private DoubleMatrix avg;
//	private DoubleMatrix predictionsMat;
//	private DoubleMatrix temperatureFactors;
//	private DoubleMatrix cloudFactors;	
//
//	// collects no production timeSlots
//	private ArrayList<Integer> prod0Slot;
//
//	//variables foe logs, can be deleted
//	private double naiveSum = 0;
//	private double errorSum = 0;
//	private double predCount = 0;
//
//
//	/**
//	 * constructs a predictor based on bootstrap data
//	 * @param cbd
//	 */
//	SolarPredictor(CustomerBootstrapData cbd)
//	{
//		m = 2000;
//		temperatureFactors = new DoubleMatrix(new double[] {0.01, 0.2, 0.5, 0.8, 1});
//		cloudFactors= new DoubleMatrix(new double[] {1});
//		prod0Slot = new ArrayList<Integer>();
//		predictionsMat = new DoubleMatrix(temperatureSeperators.length, cloudSeperators.length);
//		getPredictionMat();
//		elemCount = DoubleMatrix.zeros(temperatureSeperators.length, cloudSeperators.length);
//		avg = DoubleMatrix.zeros(temperatureSeperators.length, cloudSeperators.length);
//		ID = IDS;
//		IDS++;
//		netUsage = new HashMap<Integer, Double>();
//		double[] boot = cbd.getNetUsage();
//		for (int i = 0; i < boot.length; i++)
//		{
//			if(boot[i] != 0)
//			{
//				processInfo(boot[i], i);
//			}
//		}
//		log.info(toString() + "created successfully");
//		uncalculatedProductions = RECALCULATE + 1;
//
//		//TODO delete test
//		DoubleMatrix testVec = DoubleMatrix.zeros(3);
//		DoubleMatrix A = new DoubleMatrix(new double[] {0,1,1000});
//		DoubleMatrix B = new DoubleMatrix(new double[] {10,2,1000});
//		double c = m;
//		m = 1;
//		alter(0, testVec, A , new DoubleMatrix(new double[] {1,1,0}), B);
//		log.info("should be [2,0,0]:" + testVec);
//		m = c;
//	}
//
//	/**
//	 *  calculates best fit for alterVec[alterIndex] in the equation:
//	 *  cellCount*combinedFactorVec = cellCount*constVect * alterVec[alterIndex]
//	 *  or C*A = C*B*X
//	 *  tested to work for vectors, not guaranteed to work with A B or C matrix 
//	 * @param alterIndex place altered in the vector
//	 * @param alterVec vector in which a place will be altered
//	 * @param constVec 
//	 * @param cellCount
//	 * @param predictionVec - vector with values to predict
//	 */
//	private void alter(int alterIndex, DoubleMatrix alterVec,
//			DoubleMatrix constVec, DoubleMatrix cellCount,
//			DoubleMatrix predictionVec)
//	{
//		DoubleMatrix constLocal = constVec;
//		//		DoubleMatrix countLocal = cellCount;
//		//		if(constLocal.isRowVector())
//		//		{
//		//			constLocal = constVec.transpose();
//		//		}
//		log.info(toString() + " altering vec with \nelemCount:" + cellCount + " \nvec:" + constLocal + "\nfactors: " + predictionVec);
//		//		log.info("before altering:" + alterVec.get(alterIndex));
//		DoubleMatrix A = cellCount.mul(constLocal.mul(m));
//		DoubleMatrix B = cellCount.mul(predictionVec);
//		log.info("complex multiplication works\nA:" + A + "\nB: " + B);
//		log.info("jblas solution:" + Solve.solveLeastSquares(A, B));
//		alterVec.put(alterIndex, Solve.solveLeastSquares(A, B).get(0)); 
//		//		log.info("solved cell: " + alterVec.get(alterIndex));
//	}
//
//	/**
//	 * alters m 
//	 * @param alterIndex
//	 */
//	private void alter()
//	{
//		//		m = avg.mul(elemCount).sum()/elemCount.sum();
//		//		log.info("new m is now: " + m + "\nour m is now:" + predictionsMat.mul(elemCount).sum()/elemCount.sum());
//		//		log.info("factors is" + temperatureFactors.mmul(cloudFactors.transpose());
//		DoubleMatrix A = getFactorMat().mul(elemCount).reshape(predictionsMat.getLength(), 1);
//		DoubleMatrix B = predictionsMat.mul(elemCount).reshape(predictionsMat.getLength(), 1);
//		log.info("after reshaping they look like:" +A + " factors: " + B);
//		m = Solve.solveLeastSquares(A, B).get(0);
//		log.info("jblas m sol:" + Solve.solveLeastSquares(A, B));
//		log.info("solving for m\nA:" + getFactorMat() + "\nB: " + predictionsMat + "\nnew m :" + m);
//	}
//	private void removeNaN(DoubleMatrix mat) {
//		for(int i = 0; i < mat.length; i++)
//		{
//			mat.put(i, Double.isNaN(mat.get(i)) ? 0 : mat.get(i)); 
//		}
//
//	}
//
//	/**
//	 * combines the temperature and cloud factors with the mean to get a prediction matrix
//	 */
//	private void getPredictionMat()
//	{
//		predictionsMat = getFactorMat();
//		predictionsMat.muli(m);
//		log.info("prediction matrix: " + predictionsMat);
//	}
//
//	/**
//	 * @return the matrix of temperatureFactors*cloudFactors.transpose()
//	 */
//	private DoubleMatrix getFactorMat() {
//		//		for(int i = 0; i < temperatureFactors.length; i++)
//		//		{
//		//			predictionsMat.putRow(i, cloudFactors.mul(temperatureFactors.get(i)));
//		//		}
//		log.info("matrix calculated from factors: " + temperatureFactors.mmul(cloudFactors.transpose()));
//		return temperatureFactors.mmul(cloudFactors.transpose());
//	}
//
//	@Override
//	public double predict(int timeSlot) {
//		//		log.info(toString() + "predict recalculates? " +(uncalculatedProductions > RECALCULATE) + " uncalculatedProductions " + uncalculatedProductions);
//		if(uncalculatedProductions > RECALCULATE)
//		{
//			reCalculate();
//		}
//		double result = -1;
//		try
//		{
//			//		 //if you want to get just the average as a prediction
//			//		result = avg.get(calculateRange(temperatureSeperators, WeatherPredictor.getTemperature(timeSlot)),
//			//				calculateRange(cloudSeperators, WeatherPredictor.getCloud(timeSlot)));
//			result = predictionsMat.get(calculateRange(temperatureSeperators, WeatherPredictor.getTemperature(timeSlot)),
//					calculateRange(cloudSeperators, WeatherPredictor.getCloud(timeSlot)));
//		}
//		catch (ArrayIndexOutOfBoundsException e) 
//		{
//			result = -1;
//		}
//		log.info("full report:" + "\nelemCount:" + elemCount + "\navg:" + avg + "\nprediction mat" + predictionsMat + "\nprediction average error: " +
//				(errorSum/predCount) + "\nnaive average error: " + (naiveSum/predCount));
//		if (result == -1)
//		{
//			log.error(toString() + "unable to predict, probably no forecast was given");
//			try
//			{
//				result = avg.get(calculateRange(temperatureSeperators, WeatherPredictor.getTemperature(timeSlot)),
//						calculateRange(cloudSeperators, WeatherPredictor.getCloud(timeSlot)));
//			}
//			catch (ArrayIndexOutOfBoundsException e) 
//			{
//				result = -1;
//			}
//		}
//		return (result == -1) ? getAverage() : result;
//	}
//
//	/**
//	 * 
//	 * @return average over all productions seen 
//	 */
//	private double getAverage() {
//		return avg.mul(elemCount).sum()/elemCount.sum();
//	}
//
//	/**
//	 * calculates the formula that gets temperature and cloud and returns a production
//	 */
//	private void reCalculate() {
//		log.info(toString() + "recalculating formula");
//		updateMatrices();
//		for (int j = 0; j < ALTER_REPS; j++)
//		{
//			alter();
//			assert(avg.rows == temperatureFactors.length);
//			assert(avg.columns == cloudFactors.length);
//			elemCount.assertSameSize(avg);
//			for (int i = 1; i < cloudFactors.length; i++)
//			{
//				log.info("factors before:" + cloudFactors);
//				alter(i, cloudFactors, temperatureFactors, elemCount.getColumn(i), avg.getColumn(i));
//				log.info("factors after:" + cloudFactors);
//			}
//			for (int i = 1; i < temperatureFactors.length; i++)
//			{
//				alter(i, temperatureFactors, cloudFactors, elemCount.getRow(i).transpose(), avg.getRow(i).transpose());
//			}
//		}
//		log.info("temperature Factors:" + temperatureFactors + "cloud Factors" + cloudFactors);
//		getPredictionMat();
//		log.info("predictions now: " + predictionsMat);
//	}
//
//	/**
//	 * adds the new information gathered to the needed matrices
//	 */
//	private void updateMatrices() {
//		log.info("0 production info:\ntimeSlot\ttemperature\tdir\ttemperatureRange\tdirRane");
//		for (Integer elem : prod0Slot)
//		{
//			log.info("\t" + elem + "\t" + WeatherPredictor.getTemperature(elem) + "\t" + WeatherPredictor.getCloud(elem) + "\t"
//					+ calculateRange(temperatureSeperators, WeatherPredictor.getTemperature(elem)) + "\t" + calculateRange(cloudSeperators, WeatherPredictor.getCloud((elem))));
//		}
//		// make avg a sum matrix
//		//		log.info("matrix before" + avg);
//		avg = avg.mul(elemCount);
//		//		log.info("sum matrix" + avg);
//		//add new values
//		for (Map.Entry<Integer, Double> entry : netUsage.entrySet())
//		{
//			int temperatureRange = calculateRange(temperatureSeperators, WeatherPredictor.getTemperature(entry.getKey()));
//			int cloudRange = calculateRange(cloudSeperators, WeatherPredictor.getCloud((entry.getKey())));
//			// make sure weather report exists
//			if(temperatureRange != WeatherPredictor.FAIL && cloudRange != WeatherPredictor.FAIL)
//			{
//				//increment count
//				elemCount.put(temperatureRange, cloudRange, elemCount.get(temperatureRange, cloudRange) + 1);
//
//				//add new value to the sums matrix
//				avg.put(temperatureRange, cloudRange, avg.get(temperatureRange, cloudRange) + entry.getValue());
//
////				log.info("added weather report for timeSlot" + entry.getKey() + "\nvalue = " + entry.getValue() + "\nwould have predicted:" + predict(entry.getKey()));
//				//TODO delete this zeroing up, it is done later anyway (needed for recursive call for predict)
//				uncalculatedProductions = 0;
//				errorSum += entry.getValue() - predict(entry.getKey());
//				predCount++;
//				naiveSum += avg.get(calculateRange(temperatureSeperators, WeatherPredictor.getTemperature(entry.getKey())),
//						calculateRange(cloudSeperators, WeatherPredictor.getCloud(entry.getKey())))/elemCount.get(calculateRange(temperatureSeperators, WeatherPredictor.getTemperature(entry.getKey())),
//								calculateRange(cloudSeperators, WeatherPredictor.getCloud(entry.getKey())));				
//			}
//			else
//			{
//				log.error("no weather report found for timeSlot" + entry.getKey());
//			}
//		}
//		//		log.info("new sum matrix" + avg);
//		//divide the matrix to make it avg matrix again instead of sum matrix
//		avg = avg.div(elemCount);
//		removeNaN(avg);
//		//clean updated values
//		allInfoUsed();		
//		log.info(toString() + "avgs matrix calculated");
//		//		log.info(avg);
//		//		log.info(elemCount);
//	}
//
//	private void allInfoUsed() {
//		netUsage.clear();
//		uncalculatedProductions = 0;		
//	}
//
//	/**
//	 * returns the lowest cell that is bigger than than the key
//	 * assumes array is sorted 
//	 * assumes key is smaller than the biggest cell
//	 * @param temperatureSeperators2
//	 * @param key
//	 * @return number of lowest cell,
//	 * WeatherPredictor.FAIL otherwise (the FAIL value is guarenteed not to be a legal num)
//	 */
//	private int calculateRange(int[] array, double key) {
//		if (key < 0)
//		{
//			log.error("no data");
//			return WeatherPredictor.FAIL;
//		}
//		for(int i = 0; i < array.length; i++)
//		{
//			if (key <= array[i])
//			{
//				return i;
//			}
//		}
//		return WeatherPredictor.FAIL;
//	}
//
//	@Override
//	public void processInfo(double production, int timeSlot) {
//		uncalculatedProductions++;
//		netUsage.put(timeSlot, production);
//
//
//		if (production == 0)
//		{
//			prod0Slot.add(timeSlot);
//		}
//
//		log.info(toString() + "got production info" + netUsage.get(timeSlot) 
//				+ "in timeSlot" + timeSlot + "uncalculated productions:" + uncalculatedProductions);
//	}
//	//	@Override
//	//	public void processInfo(WeatherForecast wf) {
//	//		log.info(toString() + "forcast");
//	//		super.processInfo(wf);
//	//		
//	//
//	//	}
//	//	@Override
//	//	public void processInfo(WeatherReport wf) {
//	//		log.info(toString() + "got report");
//	//		super.processInfo(wf);
//	//		log.info("succesfully, temperature recorded:"+temperaturesSeen.get(wf.getTimeslotIndex()));
//	//		
//	//		
//	//	}
//	@Override
//	public String toString()
//	{
//		return "#" + ID + " solar predictor";
//	}
//
//}
