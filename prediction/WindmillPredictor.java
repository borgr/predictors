package prediction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.jblas.DoubleMatrix;
import org.jblas.Solve;
import org.powertac.common.msg.CustomerBootstrapData;


public class WindmillPredictor extends WeatherPredictor implements Predictor {
	static int IDS = 0;
	/*currently most of the log words represent the function they are in,
	 * some are special though like fullRep that prints most of the general interesting information   
	 */
	//TODO all log words: "0prod", "recalc", "cbd", "fullRep", "alterM", "testAlterVec",
	// "alterVec", "updateMat", "predict", "calcRange", "ctor", "updatePredictionMat"
	static final String[] printing = {"recalc", "cbd", "fullRep", "alterM", "testAlterVec", "predict"};
	private int ID;
	///recalculate after RECALCULATE times
	private final int RECALCULATE = 10;
	///how many recalculations repetitions to do
	private final int ALTER_REPS = 10;
	///hash of timeSlot and production
	private HashMap<Integer, Double> netUsage;
	private int uncalculatedProductions;
	static int[] speedSeperators = {1,3,7,10,15}; //upper bounds of ranges
	static int[] directionSeperators = {90,180,270,360};
	///  expected mean
	double m;
	private final double DEFAULT_M = 0; // it is the first value guesses so changing it is irrelevant
	private DoubleMatrix elemCount;
	///  average productions matrix, each cell is the average production with given speed and direction   
	private DoubleMatrix avg;
	///  result matrix, each cell has a prediction
	private DoubleMatrix predictionsMat;
	private DoubleMatrix speedFactors;
	private DoubleMatrix directionFactors;	

	//used for testings only:
	// collects timeSlots with no production 
	private ArrayList<Integer> prod0Slot;
	//variables for logs, can be deleted
	private double naiveSum = 0;
	private double errorSum = 0;
	private double predCount = 0;

	public WindmillPredictor(CustomerBootstrapData cbd, double population) {
		m = DEFAULT_M;
		speedFactors = new DoubleMatrix(new double[] {0.5,0.5,1,1.2,0.01});
		directionFactors= new DoubleMatrix(new double[] {0.5,0.8,1,0.9});
		prod0Slot = new ArrayList<Integer>();
		predictionsMat = new DoubleMatrix(speedSeperators.length, directionSeperators.length);
		updatePredictionMat();
		elemCount = DoubleMatrix.zeros(speedSeperators.length, directionSeperators.length);
		avg = DoubleMatrix.zeros(speedSeperators.length, directionSeperators.length);
		ID = IDS;
		IDS++;
		netUsage = new HashMap<Integer, Double>();
		double[] boot = cbd.getNetUsage();
		for (int i = 0; i < boot.length; i++)
		{
			if(boot[i] != 0)
			{
				processInfo(boot[i]/population, i);
				logIt(boot[i]/population + "boot pop" + population, "cbd");
			}
		}
		logIt(toString() + "created successfully", "ctor");
		uncalculatedProductions = RECALCULATE + 1;

		//TODO test may be deleted
		DoubleMatrix testVec = DoubleMatrix.zeros(3);
		DoubleMatrix A = new DoubleMatrix(new double[] {0,1,1000});
		DoubleMatrix B = new DoubleMatrix(new double[] {10,2,1000});
		double c = m;
		m = 1;
		alter(0, testVec, A , new DoubleMatrix(new double[] {1,1,0}), B);
		logIt("should be [2,0,0]:" + testVec, "testAlterVec");
		m = c;
	}

	/**
	 *  calculates best fit for alterVec[alterIndex] in the equation:
	 *  cellCount*combinedFactorVec = cellCount*constVect * alterVec[alterIndex]
	 *  or C*A = C*B*X
	 *  tested to work for vectors, not guaranteed to work with A B or C matrix 
	 * @param alterIndex place altered in the vector
	 * @param alterVec vector in which a place will be altered
	 * @param constVec 
	 * @param cellCount
	 * @param predictionVec - vector with values to predict
	 */
	private void alter(int alterIndex, DoubleMatrix alterVec,
			DoubleMatrix constVec, DoubleMatrix cellCount,
			DoubleMatrix predictionVec)
	{
		DoubleMatrix constLocal = constVec;
		logIt(toString() + " altering vec with \nelemCount:" + cellCount + " \nvec:" + constLocal + "\nfactors: " + predictionVec, "alterVec");
		logIt("before altering:" + alterVec.get(alterIndex), "alterVec");
		DoubleMatrix A = cellCount.mul(constLocal.mul(m));
		DoubleMatrix B = cellCount.mul(predictionVec);
		logIt("complex multiplication works\nA:" + A + "\nB: " + B, "alterVec");
		logIt("jblas solution:" + Solve.solveLeastSquares(A, B), "alterVec");
		alterVec.put(alterIndex, Solve.solveLeastSquares(A, B).get(0)); 
		logIt("solved cell: " + alterVec.get(alterIndex), "alterVec");
	}

	/**
	 * alters m 
	 * @param alterIndex
	 */
	private void alter()
	{
		if (m == DEFAULT_M)
		{
			//starts with avg as first guess
			m = getAverage();
			updatePredictionMat();
			logIt("m initiated to:" + m, "alterM");
		}
		else
		{
			logIt("new m is now: " + m + "\nour m is now:" + predictionsMat.mul(elemCount).sum()/elemCount.sum(), "alterM");
			logIt("factors is" + speedFactors.mmul(directionFactors.transpose()), "alterM");
			DoubleMatrix A = getFactorMat().mul(elemCount).reshape(predictionsMat.getLength(), 1);
			DoubleMatrix B = predictionsMat.mul(elemCount).reshape(predictionsMat.getLength(), 1);
			logIt("after reshaping they look like:" +A + " factors: " + B, "alterM");
			m = Solve.solveLeastSquares(A, B).get(0);
			logIt("jblas m sol:" + Solve.solveLeastSquares(A, B), "alterM");
			logIt("solving for m\nA:" + getFactorMat() + "\nB: " + predictionsMat + "\nnew m :" + m, "alterM");
		}
	}
	private void removeNaN(DoubleMatrix mat) {
		for(int i = 0; i < mat.length; i++)
		{
			mat.put(i, Double.isNaN(mat.get(i)) ? 0 : mat.get(i)); 
		}

	}

	/**
	 * combines the speed and direction factors with the mean to get a prediction matrix
	 */
	private void updatePredictionMat()
	{
		predictionsMat = getFactorMat();
		predictionsMat.muli(m);
		logIt("prediction matrix: " + predictionsMat, "updatePredictionMat");
	}

	/**
	 * @return the matrix of speedFactors*directionFactors.transpose()
	 */
	private DoubleMatrix getFactorMat() {
		return speedFactors.mmul(directionFactors.transpose());
	}

	@Override
	public double predict(int timeSlot) {
		logIt(toString() + "predict recalculates? " +(uncalculatedProductions > RECALCULATE) + " uncalculatedProductions " + uncalculatedProductions, "predict");
		if(uncalculatedProductions > RECALCULATE)
		{
			reCalculate();
			logIt("full report:" + "\nm:" + m + "\nelemCount:" + elemCount + "\navg:" + avg + "\nprediction mat" + predictionsMat + "\nprediction average error: " +
					(errorSum/predCount) + "\nnaive average error: " + (naiveSum/predCount), "fullRep");
		}


		double result = -1;
		try
		{
			result = predictionsMat.get(calculateRange(speedSeperators, WeatherPredictor.getSpeed(timeSlot)),
					calculateRange(directionSeperators, WeatherPredictor.getDirection(timeSlot)));
		}
		catch (ArrayIndexOutOfBoundsException e) 
		{
			result = -1;
		}
		//		uncomment next line if you want to get just the average as a prediction
		//		result = -1;
		if (result == -1)
		{
			logIt(toString() + "unable to predict, probably no forecast was given", "predict", true);
			try
			{
				result = avg.get(calculateRange(speedSeperators, WeatherPredictor.getSpeed(timeSlot)),
						calculateRange(directionSeperators, WeatherPredictor.getDirection(timeSlot)));
			}

			catch (ArrayIndexOutOfBoundsException e) 
			{
				result = -1;
				logIt(toString() + "unable to predict", "predict", true);
			}
		}
		return (result == -1) ? getAverage() : result;
	}

	/**
	 * 
	 * @return average over all productions seen 
	 */
	private double getAverage() {
		return avg.mul(elemCount).sum()/elemCount.sum();
	}

	/**
	 * calculates the formula that gets speed and direction and returns a production
	 */
	private void reCalculate() {
		logIt(toString() + "recalculating formula", "recalc");
		updateMatrices();
		for (int j = 0; j < ALTER_REPS; j++)
		{
			alter();
			assert(avg.rows == speedFactors.length);
			assert(avg.columns == directionFactors.length);
			elemCount.assertSameSize(avg);
			for (int i = 0; i < directionFactors.length; i++)
			{
				logIt("factors before:" + directionFactors, "recalc");
				alter(i, directionFactors, speedFactors, elemCount.getColumn(i), avg.getColumn(i));
				logIt("factors after:" + directionFactors,"recalc");
			}
			for (int i = 0; i < speedFactors.length; i++)
			{
				alter(i, speedFactors, directionFactors, elemCount.getRow(i).transpose(), avg.getRow(i).transpose());
			}
		}

		updatePredictionMat();
		logIt("speed Factors:" + speedFactors + "direction Factors" + directionFactors, "recalc");
		logIt("predictions now: " + predictionsMat, "recalc");
		if (predictionsMat.le(300).min() == 0)
		{
			logIt("big number found", "recalc");
		}
	}

	/**
	 * makes default value info
	 * @see logIt(String string, String logType, boolean error)
	 * @param string
	 * @param logType
	 */
	private void logIt(String string, String logType)
	{
		logIt(string, logType, false);
	}
	/**
	 * logs the string iff logType is in printing array
	 * @param string
	 * @param logType
	 */
	private void logIt(String string, String logType, boolean error) {
		if (Arrays.asList(printing).contains(logType))
		{
			if (error)
			{
				log.error(string);
			}
			else
			{
				log.info(string);
			}
		}

	}

	/**
	 * adds the new information gathered to the needed matrices
	 */
	private void updateMatrices() {
		logIt("0 production info:\ntimeSlot\tspeed\tdir\tspeedRange\tdirRane", "0prod");
		//		for (Integer elem : prod0Slot)
		//		{
		///**			logIt("\t" + elem + "\t" + WeatherPredictor.getSpeed(elem) + "\t" + WeatherPredictor.getDirection(elem) + "\t"
		//		+ calculateRange(speedSeperators, WeatherPredictor.getSpeed(elem)) + "\t" + calculateRange(directionSeperators, WeatherPredictor.getDirection((elem))));
		//	*/	}


		// make avg a sum matrix
		logIt("matrix before" + avg, "updateMat");
		avg = avg.mul(elemCount);
		logIt("sum matrix" + avg, "updateMat");
		//add new values
		for (Map.Entry<Integer, Double> entry : netUsage.entrySet())
		{
			try{
				int speedRange = calculateRange(speedSeperators, WeatherPredictor.getSpeed(entry.getKey()));
				int directionRange = calculateRange(directionSeperators, WeatherPredictor.getDirection((entry.getKey())));
				// make sure weather report exists
				if(speedRange != WeatherPredictor.FAIL && directionRange != WeatherPredictor.FAIL)
				{
					//				// logs when speedRange is 0 but production is not
					//				 if (speedRange == 0 && entry.getValue() != 0)
					// logs when speedRange is 0
					if (speedRange == 0)
					{
						logIt(toString() + " added " + entry.getValue() 
								+ " with speed: " + WeatherPredictor.getSpeed(entry.getKey())
								+ " and direction: " + WeatherPredictor.getDirection((entry.getKey())) + 
								"\nto cell" +speedRange+ "," + directionRange, "0prod");
					}
					//increment count
					elemCount.put(speedRange, directionRange, elemCount.get(speedRange, directionRange) + 1);

					//add new value to the sums matrix
					avg.put(speedRange, directionRange, avg.get(speedRange, directionRange) + entry.getValue());

					//uncomment both lines if you want log (avoids infinite recursion)
					//				uncalculatedProductions = 0;
					//				logIt("added weather report for timeSlot" + entry.getKey() + "\nvalue = " + entry.getValue() + "\nwould have predicted:" + predict(entry.getKey()));
					uncalculatedProductions = 0;
					errorSum += entry.getValue() - predict(entry.getKey());
					predCount++;
					naiveSum += avg.get(calculateRange(speedSeperators, WeatherPredictor.getSpeed(entry.getKey())),
							calculateRange(directionSeperators, WeatherPredictor.getDirection(entry.getKey())))/elemCount.get(calculateRange(speedSeperators, WeatherPredictor.getSpeed(entry.getKey())),
									calculateRange(directionSeperators, WeatherPredictor.getDirection(entry.getKey())));				
				}
				else
				{
					logIt("no weather report found for timeSlot" + entry.getKey(), "updateMat", true);
				}
			}
			catch (Exception e)
			{
				logIt("entry error, probably exceeded separator bound", "updateMat", true);
			}
		}
		//divide the matrix to make it avg matrix again instead of sum matrix
		avg = avg.div(elemCount);
		removeNaN(avg);
		//clean updated values
		allInfoUsed();		
	}

	private void allInfoUsed() {
		netUsage.clear();
		uncalculatedProductions = 0;		
	}

	/**
	 * returns the lowest cell that is bigger than than the key
	 * assumes array is sorted 
	 * assumes key is smaller than the biggest cell
	 * @param speedSeperators2
	 * @param key
	 * @return number of lowest cell,
	 * WeatherPredictor.FAIL otherwise (the FAIL value is guarenteed not to be a legal num)
	 */
	private int calculateRange(int[] array, double key) {
		if (key < 0)
		{
			logIt("no data", "calcRange" ,true);
			return WeatherPredictor.FAIL;
		}
		for(int i = 0; i < array.length; i++)
		{
			if (key <= array[i])
			{
				logIt("key smallerequal than" + key + "this"+ array[i], "calcRange");
				return i;
			}
		}
		return WeatherPredictor.FAIL;
	}

	@Override
	public void processInfo(double production, int timeSlot) {
		uncalculatedProductions++;
		netUsage.put(timeSlot, production);


		if (production == 0)
		{
			prod0Slot.add(timeSlot);
		}
	}
	@Override
	public String toString()
	{
		return "#" + ID + " windmill predictor";
	}

}

