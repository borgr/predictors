package prediction;

import java.util.HashMap;
import org.apache.log4j.Logger;
import org.powertac.common.CustomerInfo;
import org.powertac.common.TariffTransaction;
import org.powertac.common.TimeService;
import org.powertac.common.WeatherForecast;
import org.powertac.common.WeatherReport;
import org.powertac.common.enumerations.PowerType;
import org.powertac.common.msg.CustomerBootstrapData;
import org.powertac.common.repo.CustomerRepo;
import org.powertac.common.repo.TariffRepo;
import org.powertac.common.repo.TimeslotRepo;
import org.powertac.samplebroker.PortfolioManagerService;
import org.powertac.samplebroker.interfaces.Activatable;
import org.powertac.samplebroker.interfaces.BrokerContext;
import org.powertac.samplebroker.interfaces.Initializable;
import org.powertac.samplebroker.interfaces.MarketManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service // Spring creates a single instance at startup
public class PredictionGenerator implements Initializable, Activatable{
	static private Logger log = Logger.getLogger(PortfolioManagerService.class);

	//	private BrokerContext brokerContext; // master

	// Spring fills in Autowired dependencies through a naming convention
	@Autowired
	private TimeslotRepo timeslotRepo;

	@Autowired
	private TariffRepo tariffRepo;

	@Autowired
	private CustomerRepo customerRepo;

	@Autowired
	private MarketManager marketManager;

	@Autowired
	private TimeService timeService;

	///maps from customer names to predictors
	private HashMap<String, Predictor> predictors;

	public PredictionGenerator() {
		log.info("prediction created");
		predictors = new HashMap<String, Predictor>();
	}

	/**
	 * Per-game initialization. Configures parameters and registers
	 * message handlers.
	 */
	@Override // from Initializable
	//  @SuppressWarnings("unchecked")
	public void initialize (BrokerContext context)
	{
		//		this.brokerContext = context;
		log.info("prediction initialized");

		//	    customerProfiles = new HashMap<PowerType,
		//	        HashMap<CustomerInfo, CustomerRecord>>();
		//	    customerSubscriptions = new HashMap<TariffSpecification,
		//	        HashMap<CustomerInfo, CustomerRecord>>();
		//	    competingTariffs = new HashMap<PowerType, List<TariffSpecification>>();
	}

	public boolean handleMessage(CustomerBootstrapData cbd, CustomerInfo customer) {
		double population = customer.getPopulation();
		if(cbd.getPowerType() == PowerType.WIND_PRODUCTION)
		{
			try{
				Predictor value = new WindmillPredictor(cbd, population);
				log.info("prediction got bootstrap report" + cbd.getCustomerName());
				predictors.put(cbd.getCustomerName(), value);
				log.info("predictor created: ");
				log.info(predictors.get(cbd.getCustomerName()));
			}
			catch(Exception e){
				log.error(e.getMessage());
				log.error("unable to initialize wind predictor");
			}
			return true;
		}
		else if(cbd.getPowerType() == PowerType.SOLAR_PRODUCTION)
		{
			try{
				Predictor value = new SolarPredictor(cbd, population);
				log.info("prediction got bootstrap report" + cbd.getCustomerName());
				predictors.put(cbd.getCustomerName(), value);
				log.info("predictor created: " + predictors.get(cbd.getCustomerName()));
			}
			catch(Exception e){
				log.error(e.getMessage());
				log.error("unable to initialize solar predictor");
			}
			return true;
		}
		return false;
		
	}

	//this api is better but this is a temp solution,
	//when cbd is enough to find a costumer info, this methos should replace 
	//handleMessage(CustomerBootstrapData cbd, CustomerInfo customer)
//	public boolean handleMessage(CustomerBootstrapData cbd)
//	{
//		if(cbd.getPowerType() == PowerType.WIND_PRODUCTION)
//		{
//			try{
//				Predictor value = new WindmillPredictor(cbd);
//				log.info("prediction got bootstrap report" + cbd.getCustomerName());
//				predictors.put(cbd.getCustomerName(), value);
//				log.info("predictor created: ");
//				log.info(predictors.get(cbd.getCustomerName()));
//			}
//			catch(Exception e){
//				log.error(e.getMessage());
//				log.error("unable to initialize wind predictor");
//			}
//			return true;
//		}
//		else if(cbd.getPowerType() == PowerType.SOLAR_PRODUCTION)
//		{
//			try{
//				Predictor value = new SolarPredictor(cbd);
//				log.info("prediction got bootstrap report" + cbd.getCustomerName());
//				predictors.put(cbd.getCustomerName(), value);
//				log.info("predictor created: " + predictors.get(cbd.getCustomerName()));
//			}
//			catch(Exception e){
//				log.error(e.getMessage());
//				log.error("unable to initialize solar predictor");
//			}
//			return true;
//		}
//		return false;
//	}
	
	/**
	 * handles a weather forecast message
	 * @param wf
	 */
	public void handleMessage(WeatherForecast wf)
	{
		//		for (Map.Entry<String, Predictor> entry : predictors.entrySet())
		//		{
		//			entry.getValue().processInfo(wf);
		//		}
		WeatherPredictor.processWeatherInfo(wf);
	}
	/**
	 * handles tariff transaction message
	 * @param ttx
	 */
	public void handleMessage(TariffTransaction ttx) {
		log.info("prediction got ttx");		
		log.info("sending to " + ttx.getCustomerInfo().getName() +
				"KWH/population" + ttx.getKWh()/ttx.getCustomerCount() +
				"timeslot" + ttx.getPostedTimeslot().getSerialNumber() + "costumer count" + ttx.getCustomerCount());
		predictors.get(ttx.getCustomerInfo().getName()).processInfo(
				ttx.getKWh()/ttx.getCustomerCount(),
				ttx.getPostedTimeslot().getSerialNumber());
	}

	public void processInfo(WeatherReport wr)
	{
		log.info("prediction got weather report");
		log.info("num of predictors:" + predictors.size());
		//		for (Map.Entry<String, Predictor> entry : predictors.entrySet())
		//		{
		//			log.info("sending wr to ");
		//			log.info(entry.getKey());
		//			entry.getValue().processInfo(wr);
		//		}
		WeatherPredictor.processWeatherInfo(wr);
	}
	public double getPrediction(CustomerInfo ci, int timeSlot)
	{
		return getPrediction(ci.getName(), timeSlot); 
	}

	/**
	 * predicts the production for the customer in the time slot
	 * @param customerName
	 * @param timeSlot
	 * @return Double.NEGATIVE_INFINITY if the customer was never created
	 */
	private double getPrediction(String customerName, int timeSlot) {
		Predictor predictor = predictors.get(customerName);
		//		log.info("prediction generator passes prediction to " + customerName);
		return predictor == null ? Double.NEGATIVE_INFINITY : predictor.predict(timeSlot);
	}

	@Override
	public void activate(int timeslot) {
		// TODO Auto-generated method stub
	}

}
